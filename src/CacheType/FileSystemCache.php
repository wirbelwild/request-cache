<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\CacheType;

use BitAndBlack\Composer\VendorPath;
use BitAndBlack\Duration\Duration;
use BitAndBlack\PathInfo\PathInfo;
use BitAndBlack\RequestCache\Exception\CorruptFileException;
use BitAndBlack\RequestCache\Exception\InvalidArgumentException;
use BitAndBlack\RequestCache\Request;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Message;
use JsonException;
use Laravel\SerializableClosure\Exceptions\PhpVersionNotSupportedException;
use Laravel\SerializableClosure\SerializableClosure;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Finder\Finder;

/**
 * The FileSystemCache class allows storing cached data on the file system.
 *
 * @package BitAndBlack\RequestCache
 * @see \BitAndBlack\RequestCache\Tests\CacheType\FileSystemCacheTest
 */
class FileSystemCache implements CacheTypeInterface
{
    private string $cacheFolder;

    private string $suffix = 'php';

    private string $tempName = '-bitandblack-request-cache-temp-file';
    
    /**
     * @var callable
     */
    private $requestCallback;

    private LoggerInterface $logger;

    /**
     * FileSystemCache constructor.
     *
     * @param string $cacheFolder Path to the cache folder, where all requests may be stored.
     */
    public function __construct(string $cacheFolder)
    {
        $this->logger = new NullLogger();
        $this->cacheFolder = $cacheFolder;
        $this->requestCallback = static function (
            string $url,
            string $method = 'GET',
            array $headers = [],
            $body = null,
            string $version = '1.1'
        ): ResponseInterface {
            $client = new Client();
            $options = [
                'headers' => array_merge(
                    $headers,
                    [
                        'http_errors' => false,
                        'allow_redirects' => true,
                    ],
                ),
                'body' => $body,
                'version' => $version,
            ];

            return $client->request(
                $method,
                $url,
                $options
            );
        };
    }

    /**
     * @param string $suffix Name of the suffix.
     * @return FileSystemCache
     */
    public function setSuffix(string $suffix): self
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @param string $tempName Additional name part for temp files.
     * @return FileSystemCache
     */
    public function setTempName(string $tempName): self
    {
        $this->tempName = $tempName;
        return $this;
    }

    /**
     * @param string $key
     * @param null $default
     * @return ResponseInterface|mixed
     * @throws InvalidArgumentException
     * @throws CorruptFileException
     */
    public function get($key, $default = null)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, 'string');
        }

        $cacheFile = $this->getFilePath($key);
        
        if (!file_exists($cacheFile)) {
            $this->logger->debug('Default value has been returned because cache file "' . PathInfo::createFromFile($cacheFile)->getFileName() . '" doesn\'t exist.');
            return $default;
        }
        
        $content = (string) file_get_contents($cacheFile);

        try {
            $content = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $jsonException) {
            throw new CorruptFileException($cacheFile);
        }

        $content = false !== $content
            ? $content
            : []
        ;
        
        if (!isset($content['value']) || empty($content['value'])) {
            $this->logger->debug('Missed content in cache file "' . PathInfo::createFromFile($cacheFile)->getFileName() . '".');
            return $default;
        }

        $value = base64_decode($content['value']);

        $this->logger->debug('Content of cache file "' . PathInfo::createFromFile($cacheFile)->getFileName() . '" has been requested and returned.');

        return Message::parseResponse($value);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int|float|null $ttl
     * @return bool
     * @throws InvalidArgumentException
     */
    public function set($key, $value, $ttl = null): bool
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, 'string');
        }
        
        if (!$value instanceof ResponseInterface) {
            throw new InvalidArgumentException($value, ResponseInterface::class);
        }

        if (null === $ttl) {
            $ttl = Duration::createFromDays(1)->getSeconds();
        }

        $value = Message::toString($value);
        $cacheFile = $this->getFilePath($key);
        
        $values = [
            'value' => base64_encode($value),
            'ttl' => $ttl,
        ];

        $pathInfo = new PathInfo($cacheFile);
        $dirName = $pathInfo->getDirName();

        if (null !== $dirName && !file_exists($dirName)) {
            mkdir($dirName);
        }

        $content = file_put_contents(
            $cacheFile,
            json_encode($values, JSON_PRETTY_PRINT)
        );

        $isTempCacheFile = false;

        if (str_ends_with($key, $this->tempName)) {
            $isTempCacheFile = true;
        }

        $this->logger->debug('Added file "' . PathInfo::createFromFile($cacheFile)->getFileName() . '" for key "' . $key . '"' . ($isTempCacheFile ? ' (internal temp file)' : null) . '.');
        
        return false !== $content;
    }

    /**
     * @param string $key
     * @return bool
     * @throws InvalidArgumentException
     */
    public function delete($key): bool
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, 'string');
        }

        $cacheFile = $this->getFilePath($key);

        $isTempCacheFile = false;

        if (str_ends_with($key, $this->tempName)) {
            $isTempCacheFile = true;
        }

        if (file_exists($cacheFile)) {
            $this->logger->debug('Removed file "' . PathInfo::createFromFile($cacheFile)->getFileName() . '"' . ($isTempCacheFile ? ' (internal temp file)' : null) . '.');
            return unlink($cacheFile);
        }
        
        return false;
    }

    /**
     * @return bool
     * @throws InvalidArgumentException
     */
    public function clear(): bool
    {
        $finder = Finder::create()
            ->files()
            ->in($this->cacheFolder)
            ->name('*.' . $this->suffix)
        ;
        
        $success = true;
        
        foreach ($finder as $file) {
            $success &= $this->delete($file);
        }
        
        return (bool) $success;
    }

    /**
     * @param iterable<string, string> $keys
     * @param mixed $default
     * @return array<string, ResponseInterface|mixed>
     * @throws CorruptFileException
     * @throws InvalidArgumentException
     */
    public function getMultiple($keys, $default = null): array
    {
        $output = [];
        
        foreach ($keys as $key) {
            if (!is_string($key)) {
                throw new InvalidArgumentException($key, 'string');
            }

            $output[$key] = $this->get($key, $default);
        }
        
        return $output;
    }

    /**
     * @param iterable<string, string> $values
     * @param int|float|null $ttl
     * @return bool
     * @throws InvalidArgumentException
     */
    public function setMultiple($values, $ttl = null): bool
    {
        $success = true;

        foreach ($values as $key => $value) {
            $success &= $this->set($key, $value, $ttl);
        }

        return (bool) $success;
    }

    /**
     * @param iterable<int, string> $keys
     * @return bool
     * @throws InvalidArgumentException
     */
    public function deleteMultiple($keys): bool
    {
        $success = true;

        foreach ($keys as $key) {
            $success &= $this->delete($key);
        }

        return (bool) $success;
    }

    /**
     * @param string $key
     * @return bool
     * @throws InvalidArgumentException
     */
    public function has($key): bool
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, 'string');
        }
        
        $cacheFile = $this->getFilePath($key);
        return file_exists($cacheFile);
    }

    /**
     * Returns the path to a request cache file.
     *
     * @param string $requestURI
     * @return string
     */
    public function getFilePath(string $requestURI): string
    {
        $hash = hash('sha1', $requestURI);
        return $this->cacheFolder . DIRECTORY_SEPARATOR . $hash . '.' . $this->suffix;
    }

    /**
     * @param callable $requestCallback
     * @return $this
     */
    public function setRequestCallback(callable $requestCallback): self
    {
        $this->requestCallback = $requestCallback;
        return $this;
    }

    /**
     * @param string $url
     * @return ResponseInterface
     */
    public function getRequestCallback(
        string $url,
        string $method = 'GET',
        array $headers = [],
        $body = null,
        string $version = '1.1'
    ): ResponseInterface {
        return ($this->requestCallback)($url, $method, $headers, $body, $version);
    }

    /**
     * @param Request $request
     * @param CacheTypeInterface $cache
     * @return string
     * @throws PhpVersionNotSupportedException
     */
    public function getProcessCommand(Request $request, CacheTypeInterface $cache): string
    {
        $closure = fn () => $cache;
        $serializableClosure = new SerializableClosure($closure);

        /**
         * Serializing the object allows some kind of dependency injection here.
         * Using base64_encode helps keep the object together.
         */
        $cacheSerialized = serialize($serializableClosure);
        $cacheEncoded = base64_encode($cacheSerialized);
        $key = $request->getKey();
        $uri = $request->getUri();
        $method = $request->getMethod();
        $headers = $request->getHeaders();
        $body = $request->getBody();
        $protocol = $request->getProtocolVersion();
        
        $command = '
            require \'' . new VendorPath() . DIRECTORY_SEPARATOR . 'autoload.php\';
            
            $unserialized = unserialize(
                base64_decode(\'' . $cacheEncoded . '\'),
                [\BitAndBlack\RequestCache\CacheType\CacheTypeInterface::class]
            );
            
            $closure = $unserialized->getClosure();
            $fileCache = $closure();
            
            $fileCache->set(
                \'' . $key . $this->tempName . '\',
                new GuzzleHttp\Psr7\Response(), 
                \'\' 
            );

            $response = $fileCache->getRequestCallback(
                \'' . $uri . '\', 
                \'' . $method . '\', 
                ' . var_export($headers, true) . ', 
                \'' . $body . '\', 
                \'' . $protocol . '\'
            );

            $fileCache->set(
                \'' . $key . '\',
                $response, 
                ' . $request->getTtl() . '
            );

            $fileCache->delete(
                \'' . $key . $this->tempName . '\'
            );
        ';

        return str_replace(
            ['\\', '$', '"'],
            ['\\\\', '\$', '\"'],
            $command
        );
    }

    /**
     * @param Request $request
     * @return bool
     * @throws InvalidArgumentException
     */
    public function isProcessRunning(Request $request): bool
    {
        return $this->has($request->getKey() . $this->tempName);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws JsonException
     */
    public function isExpired(Request $request): bool
    {
        $now = new DateTime('now');
        
        $cacheFile = $this->getFilePath($request->getKey());
        $ttl = 0;

        if (file_exists($cacheFile)) {
            $content = @file_get_contents($cacheFile);
            $content = false !== $content
                ? json_decode($content, true, 512, JSON_THROW_ON_ERROR)
                : []
            ;
            $ttl = $content['ttl'] ?? $ttl;
        }

        $fileMTime = @filemtime($this->getFilePath($request->getKey()));

        if (false === $fileMTime) {
            return true;
        }
        
        $dateCreated = new DateTime();
        $dateCreated->setTimestamp($fileMTime);
        
        $dateExpires = clone $dateCreated;
        $dateExpires = $dateExpires->modify('+' . $ttl . ' seconds');

        return $now > $dateExpires;
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
