<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Exception;

use BitAndBlack\RequestCache\Exception;

/**
 * Class InvalidArgumentException.
 *
 * @package BitAndBlack\RequestCache\Exception
 */
class InvalidArgumentException extends Exception implements \Psr\SimpleCache\InvalidArgumentException
{
    /**
     * InvalidArgumentException constructor.
     *
     * @param mixed $argument
     * @param string $exceptedType
     */
    public function __construct($argument, string $exceptedType)
    {
        $type = gettype($argument);
        
        if (is_object($argument)) {
            $type = get_class($argument);
        }
        
        parent::__construct(
            'Argument must be of type `' . $exceptedType . '` but is `' . $type . '` instead'
        );
    }
}
