[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/request-cache)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/5b4dfa719349427495ccb2a4500b0ea0)](https://www.codacy.com/bb/wirbelwild/request-cache/dashboard) 
[![Latest Stable Version](https://poser.pugx.org/bitandblack/request-cache/v/stable)](https://packagist.org/packages/bitandblack/request-cache)
[![Total Downloads](https://poser.pugx.org/bitandblack/request-cache/downloads)](https://packagist.org/packages/bitandblack/request-cache)
[![License](https://poser.pugx.org/bitandblack/request-cache/license)](https://packagist.org/packages/bitandblack/request-cache)

# Request Cache

Smooth caching of HTTP requested data. It runs non-blocking and uses background processes to request the data.

## Installation

This library is available for the use with [Composer](https://packagist.org/packages/bitandblack/request-cache). Add it to your project by running `$ composer require bitandblack/request-cache`.

## Usage

At first initialize a cache type and a process object, and use them to initialize the request handler object. In this example we're going to use the `FileSystemCache`:

````php
<?php

use BitAndBlack\RequestCache\CacheType\FileSystemCache;
use BitAndBlack\RequestCache\Process;
use BitAndBlack\RequestCache\RequestHandler;

$cache = new FileSystemCache(__DIR__);
$process = new Process();
$requestHandler = new RequestHandler($cache, $process);
````

The request handler is now ready to be used. Now create a request object with the URL you want to request and the time to live (ttl). This example requests the URL `https://www.bitandblack.com` and allows the data to be stored for 1 hour:

````php
<?php

use BitAndBlack\RequestCache\Request;

$request = new Request(
    'https://www.bitandblack.com',
    3600
);
````

The data can be requested now:

````php
<?php

$response = $requestHandler->getResponse($request);
````

The request handler will return the response immediately and without blocking the script. If the data has been expired, the last response will be returned from cache, while the new data will be requested in the background.

When successful, the response will be a `GuzzleHttp\Psr7\Response` object, that implements the `Psr\Http\Message\ResponseInterface`.

### Options

#### Custom request client

Per default, this library uses Guzzle to make requests. You can use the request callback to set up a custom request client or to modify the configuration.

```php
<?php

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

$cache->setRequestCallback(
    function(string $url): ResponseInterface 
    {
        $client = new Client();
        return $client->request(
            'GET',
            $url,
            [
                'allow_redirects' => false
            ]
        );
    }
);
```

### TTL

The `ttl` value may be easier to set with the help of the [bitandblack/duration](https://packagist.org/packages/bitandblack/duration) library. For example instead of writing `86400` for the duration of a whole day, you can write `Duration::createFromDays(1)->getSeconds()` then.  

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).