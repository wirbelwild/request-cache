<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Tests;

use BitAndBlack\Composer\VendorPath;
use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\RequestCache\CacheType\FileSystemCache;
use BitAndBlack\RequestCache\Exception\InvalidArgumentException;
use BitAndBlack\RequestCache\Process;
use BitAndBlack\RequestCache\Request;
use BitAndBlack\RequestCache\RequestHandler;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * Class RequestHandlerTest.
 *
 * @package BitAndBlack\RequestCache\Tests
 */
class RequestHandlerTest extends TestCase
{
    private static string $testFolder;

    protected function setUp(): void
    {
        self::$testFolder = dirname(new VendorPath()) . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'request-cache';
    }

    public static function tearDownAfterClass(): void
    {
        FileSystemHelper::deleteFolder(self::$testFolder);
    }

    /**
     * @throws InvalidArgumentException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testCanRunBlocking(): void
    {
        $request = new Request(
            'https://www.bitandblack.com',
            60
        );

        $fileSystemCache = new FileSystemCache(self::$testFolder);

        if ($fileSystemCache->has($request->getKey())) {
            $fileSystemCache->delete($request->getKey());
        }

        $process = new Process();
        $requestHandler = new RequestHandler($fileSystemCache, $process);

        $value = $requestHandler->getResponse($request, null, true);

        self::assertInstanceOf(
            ResponseInterface::class,
            $value
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testCanRunNonBlocking(): void
    {
        $defaultResponse = 'NO_DATA_EXISTING';

        $request = new Request(
            'https://www.bitandblack.com',
            60
        );

        $fileSystemCache = new FileSystemCache(self::$testFolder);

        if ($fileSystemCache->has($request->getKey())) {
            $fileSystemCache->delete($request->getKey());
        }

        $process = new Process();
        $requestHandler = new RequestHandler($fileSystemCache, $process);

        $value = $requestHandler->getResponse($request, $defaultResponse, false);

        self::assertFalse(
            $fileSystemCache->has($request->getKey())
        );

        self::assertSame(
            $defaultResponse,
            $value
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function testCanRunGuzzleAndPsr7(): void
    {
        $request = new GuzzleRequest(
            'GET',
            'https://www.bitandblack.com'
        );

        $fileSystemCache = new FileSystemCache(self::$testFolder);
        
        $process = new Process();
        $requestHandler = new RequestHandler($fileSystemCache, $process);

        $response = $requestHandler->sendRequest($request);
        
        self::assertInstanceOf(
            Response::class,
            $response
        );

        $requestGuzzle = new GuzzleRequest(
            'GET',
            'https://www.bitandblack.com'
        );
        
        $request = Request::createFromHttpRequest($requestGuzzle)
            ->setTtl(60)
        ;

        $response = $requestHandler->sendRequest($request);

        self::assertInstanceOf(
            Response::class,
            $response
        );
    }
}
