<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Exception;

use BitAndBlack\RequestCache\Exception;

/**
 * @see \BitAndBlack\RequestCache\Tests\Exception\CorruptFileExceptionTest
 */
class CorruptFileException extends Exception
{
    private string $cacheFile;

    /**
     * @param string $cacheFile
     */
    public function __construct(string $cacheFile)
    {
        $this->cacheFile = $cacheFile;

        parent::__construct('Cache file "' . basename($cacheFile) . '" is corrupt and can\'t be used. ' .
            'Please consider deleting it and creating it from new.')
        ;
    }

    /**
     * @return string
     */
    public function getCacheFile(): string
    {
        return $this->cacheFile;
    }
}
