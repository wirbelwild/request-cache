<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache;

use BitAndBlack\RequestCache\CacheType\CacheTypeInterface;
use BitAndBlack\RequestCache\Exception\InvalidArgumentException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * The RequestHandler class proofs if a requests has been cached and returns its response.
 * It also proofs if the cached response has been expired and fetches new data in the background if so.
 *
 * @package BitAndBlack\RequestCache
 * @see \BitAndBlack\RequestCache\Tests\RequestHandlerTest
 */
class RequestHandler implements ClientInterface, LoggerAwareInterface
{
    private CacheTypeInterface $cache;
    
    private Process $process;

    private LoggerInterface $logger;

    /**
     * RequestHandler constructor.
     *
     * @param CacheTypeInterface $cache The cache type object.
     * @param Process $process          The process object.
     */
    public function __construct(CacheTypeInterface $cache, Process $process)
    {
        $this->logger = new NullLogger();
        $this->cache = $cache;
        $this->process = $process;
    }

    /**
     * Returns the request response or the default value.
     *
     * @param Request $request      The request object.
     * @param mixed $default        The default return value if no data exists yet.
     * @param bool $waitForResponse If the response should be awaited or not.
     * @return ResponseInterface|mixed
     * @throws InvalidArgumentException
     */
    public function getResponse(Request $request, $default = null, bool $waitForResponse = false)
    {
        $this->logger->info('Requested "' . $request->getKey() . '".');

        $cacheResponse = null;
        $itemExists = $this->cache->has($request->getKey());
        
        if ($itemExists) {
            $cacheResponse = $this->cache->get($request->getKey());
        }

        /**
         * A cached item exists, so we can return it's content.
         * If the content has expired, we request it from new in the background.
         * The updated content will be returned in the following request then.
         */
        if (null !== $cacheResponse) {
            $this->logger->debug('Found existing cache item.');

            if ($this->cache->isExpired($request)) {
                $this->logger->debug('The cache item has expired and will be requested again.');
                $this->process->requestNew($request, $this->cache);
            }

            $this->logger->debug('Returned existing content.');
            return $cacheResponse;
        }
        
        if ($itemExists) {
            $this->cache->delete($request->getKey());
        }

        $this->logger->debug('Couldn\'t find existing cache item.');

        /**
         * No cache item exists, so we need to request it initially.
         * Now we can decide if we want to wait for the response,
         * or return the default value, as long as the request hasn't been finished.
         */
        $this->process->requestNew($request, $this->cache, $waitForResponse);
        
        if (true === $waitForResponse) {
            $this->logger->debug('Returned new content after waiting for the new content.');
            return $this->cache->get($request->getKey(), $default);
        }

        $this->logger->debug('Returned default content without waiting for the new content.');
        return $default;
    }

    /**
     * This method allows a Psr7 compatible handling.
     * An async response or caching the response are not possible here.
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws InvalidArgumentException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $cacheRequest = !$request instanceof Request
            ? Request::createFromHttpRequest($request)
            : $request
        ;

        return $this->getResponse(
            $cacheRequest,
            new Response(404),
            true
        );
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
