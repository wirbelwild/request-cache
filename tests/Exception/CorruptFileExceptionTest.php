<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Tests\Exception;

use BitAndBlack\RequestCache\Exception\CorruptFileException;
use PHPUnit\Framework\TestCase;

class CorruptFileExceptionTest extends TestCase
{
    public function testDumpsRelativePath(): void
    {
        $file = '/Users/testuser/my_project_dir/test-project/wow-this-is-a-long-path-name/cache-file-xyz.php';

        $corruptFileException = new CorruptFileException($file);

        self::assertSame(
            $file,
            $corruptFileException->getCacheFile()
        );

        self::assertStringNotContainsString(
            $file,
            (string) $corruptFileException
        );
    }
}
