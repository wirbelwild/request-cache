# Changes in Bit&Black Request Cache v1.1

## 1.1.1 2022-09-30

### Changed

-   Hide full path of corrupted file from output when throwing the `CorruptFileException` exception and show only the file's name instead.

## 1.1.0 2022-06-13

### Fixed 

-   The `FileSystemCache` class will now longer throw an `JSONException` when a file is broken. Instead, a `CorruptFileException` will be thrown with information about the broken file.

### Changed

-   The `FileSystemCache` class creates the cache folder by its own now if it's missing.

### Added 

-   The `FileSystemCache` class has now the method `getFilePath` to get the path to a specific cache file.