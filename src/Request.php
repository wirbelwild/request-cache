<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache;

use DateTime;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;

/**
 * The Request class holds information about a request.
 * It implements the `Psr\Http\Message\RequestInterface` and extends it by a TTL value.
 *
 * @package BitAndBlack\RequestCache
 * @see \BitAndBlack\RequestCache\Tests\RequestTest
 */
class Request extends GuzzleRequest implements RequestInterface
{
    private string $url;
    
    private float $ttl;

    /**
     * Request constructor.
     *
     * @param string $url The URL that should be requested.
     * @param int|float|DateTime $ttl Time in seconds, how long the requested data will be valid.
     * @param (string|string[])[] $headers Request headers
     * @param string|resource|StreamInterface|null $body Request body
     * @param string $version Protocol version
     */
    final public function __construct(
        string $url,
        $ttl,
        string $method = 'GET',
        array $headers = [],
        $body = null,
        string $version = '1.1'
    ) {
        $this->url = $url;
        $this->setTtl($ttl);

        parent::__construct(
            $method,
            $url,
            $headers,
            $body,
            $version
        );
    }

    /**
     * Takes a Psr7 request and returns a modified one containing a TTL value.
     *
     * @param RequestInterface $request
     * @return Request
     */
    public static function createFromHttpRequest(RequestInterface $request): Request
    {
        return new static(
            $request->getUri(),
            0,
            $request->getMethod(),
            $request->getHeaders(),
            $request->getBody(),
            $request->getProtocolVersion()
        );
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return sha1(
            serialize([
                (string) $this->getUri(),
                $this->getMethod(),
                $this->getHeaders(),
                (string) $this->getBody(),
                $this->getProtocolVersion(),
            ])
        );
    }

    /**
     * @return float
     */
    public function getTtl(): float
    {
        return $this->ttl;
    }

    /**
     * Sets how long the requested data will be valid.
     *
     * @param int|float|DateTime $ttl Time in seconds.
     * @return Request
     */
    public function setTtl($ttl): self
    {
        if ($ttl instanceof DateTime) {
            $ttl = Helper::getTotalSeconds($ttl);
        }
        
        $this->ttl = $ttl;
        return $this;
    }
}
