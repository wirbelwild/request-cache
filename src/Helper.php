<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache;

use DateTime;

/**
 * Class Helper.
 *
 * @package BitAndBlack\RequestCache
 */
class Helper
{
    /**
     * @param DateTime $date
     * @return float
     */
    public static function getTotalSeconds(DateTime $date): float
    {
        $now = new DateTime('now');
        return (float) $date->getTimestamp() - $now->getTimestamp();
    }
}
