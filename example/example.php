<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

/**
 * This example shows, how a request may be done.
 * The first time this script runs, the response will be null. This is because the null response comes
 * without waiting for the requested data. Null is the default value here as it is not specified differently.
 * The second time this script runs, a Psr7 response will be returned. This is the async fetched data.
 */

use BitAndBlack\Duration\Duration;
use BitAndBlack\RequestCache\CacheType\FileSystemCache;
use BitAndBlack\RequestCache\Process;
use BitAndBlack\RequestCache\Request;
use BitAndBlack\RequestCache\RequestHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Process\PhpExecutableFinder;

require '../vendor/autoload.php';

$logger = new Logger('Bit&Black Request Cache');
$logger->pushHandler(new StreamHandler('example.log'));

$request = new Request(
    'https://www.bitandblack.com',
    Duration::createFromMinutes(1)->getSeconds()
);

$phpBinaryFinder = new PhpExecutableFinder();
$phpBinaryPath = $phpBinaryFinder->find();

$cache = new FileSystemCache(__DIR__);
$cache->setLogger($logger);

$process = new Process($phpBinaryPath);

$requestHandler = new RequestHandler($cache, $process);
$requestHandler->setLogger($logger);

$response = $requestHandler->getResponse($request);

var_dump($response);
