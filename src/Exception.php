<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache;

use Throwable;

/**
 * Class Exception.
 *
 * @package BitAndBlack\RequestCache
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
