<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Tests\CacheType;

use BitAndBlack\Composer\VendorPath;
use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\RequestCache\CacheType\FileSystemCache;
use BitAndBlack\RequestCache\Exception\CorruptFileException;
use BitAndBlack\RequestCache\Exception\InvalidArgumentException;
use BitAndBlack\RequestCache\Process;
use BitAndBlack\RequestCache\Request;
use BitAndBlack\RequestCache\RequestHandler;
use Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Laravel\SerializableClosure\Exceptions\PhpVersionNotSupportedException;
use PHPUnit\Framework\TestCase;

/**
 * Class FileSystemCacheTest.
 *
 * @package Places2Be\Framework\Tests
 */
class FileSystemCacheTest extends TestCase
{
    private static string $testFolder;

    protected function setUp(): void
    {
        self::$testFolder = dirname(new VendorPath()) . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'request-cache';
    }

    public static function tearDownAfterClass(): void
    {
        if (file_exists(self::$testFolder)) {
            FileSystemHelper::deleteFolder(self::$testFolder);
        }
    }

    /**
     * @throws CorruptFileException
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function testCanHandleCache(): void
    {
        $fileSystemCache = new FileSystemCache(self::$testFolder);

        $client = new Client();
        
        $key = 'https://www.bitandblack.com';
        $value = $client->request('GET', $key);
        
        if ($fileSystemCache->has($key)) {
            $fileSystemCache->delete($key);
        }
        
        self::assertFalse(
            $fileSystemCache->has($key)
        );

        $fileSystemCache->set($key, $value);

        self::assertTrue(
            $fileSystemCache->has($key)
        );

        self::assertSame(
            $value->getStatusCode(),
            $fileSystemCache->get($key)->getStatusCode()
        );

        self::assertSame(
            (string) $value->getBody(),
            (string) $fileSystemCache->get($key)->getBody()
        );

        $fileSystemCache->delete($key);

        self::assertFalse(
            $fileSystemCache->has($key)
        );
    }

    /**
     * @dataProvider getData
     * @param string $url
     * @param int $status
     * @param string $contentType
     * @throws InvalidArgumentException
     */
    public function testCanRequestDifferentMedia(string $url, int $status, string $contentType): void
    {
        $request = new Request(
            $url,
            60
        );

        $fileSystemCache = new FileSystemCache(self::$testFolder);
        $process = new Process();
        $requestHandler = new RequestHandler($fileSystemCache, $process);

        $value = $requestHandler->getResponse($request, null, true);

        self::assertInstanceOf(
            Response::class,
            $value
        );

        self::assertSame(
            $status,
            $value->getStatusCode()
        );

        self::assertStringContainsString(
            $contentType,
            $value->getHeader('Content-Type')[0]
        );
    }

    /**
     * @throws GuzzleException
     * @throws InvalidArgumentException
     * @throws CorruptFileException
     */
    public function testCanHandleCorruptCacheFile(): void
    {
        $this->expectException(CorruptFileException::class);

        $fileSystemCache = new FileSystemCache(self::$testFolder);
        $client = new Client();

        $key = 'https://www.bitandblack.com';
        $value = $client->request('GET', $key);

        if ($fileSystemCache->has($key)) {
            $fileSystemCache->delete($key);
        }

        self::assertFalse(
            $fileSystemCache->has($key)
        );

        $fileSystemCache->set($key, $value);

        self::assertTrue(
            $fileSystemCache->has($key)
        );

        self::assertSame(
            $value->getStatusCode(),
            $fileSystemCache->get($key)->getStatusCode()
        );

        self::assertSame(
            (string) $value->getBody(),
            (string) $fileSystemCache->get($key)->getBody()
        );

        $cacheFile = $fileSystemCache->getFilePath($key);

        $contents = file_get_contents($cacheFile);
        file_put_contents($cacheFile, $contents . 'CORRUPT');

        $fileSystemCache->get($key);
    }

    /**
     * @return Generator<array{
     *     uri: string,
     *     status: int,
     *     type: string,
     * }>
     */
    public function getData(): Generator
    {
        yield [
            'uri' => 'https://www.bitandblack.com',
            'status' => 200,
            'type' => 'text/html',
        ];

        yield [
            'uri' => 'https://www.bitandblack.com/favicon.ico',
            'status' => 200,
            'type' => 'application/ico',
        ];

        /** @todo Add ugly URL */
        //yield ['https://www.example.org/?geojson=color:ff000066|width:6|geometry:{"type":"MultiLineString","coordinates":[[[16.431427001953125,47.845883513447276],[16.481552124023438,47.821915003877976]],[[16.489105224609375,47.794245273679884],[16.478118896484375,47.77855921633884]]]}&size=500x450', 200, 'image/png'];
    }

    /**
     * @throws PhpVersionNotSupportedException
     */
    public function testCommandIncludesMethodAndOptions(): void
    {
        $fileSystemCache = new FileSystemCache(self::$testFolder);

        $url = 'https://www.bitandblack.com';

        $request = new Request(
            $url,
            60,
            'POST',
            [
                'headerKey' => 'headerValue',
            ],
            'bodyParameter'
        );

        $processCommand = $fileSystemCache->getProcessCommand($request, $fileSystemCache);

        self::assertStringContainsString(
            'POST',
            $processCommand
        );

        self::assertStringContainsString(
            'headerKey',
            $processCommand
        );

        self::assertStringContainsString(
            'headerValue',
            $processCommand
        );

        self::assertStringContainsString(
            'bodyParameter',
            $processCommand
        );
    }
}
