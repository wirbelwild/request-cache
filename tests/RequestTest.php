<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\Tests;

use BitAndBlack\RequestCache\Request;
use DateTime;
use Generator;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use PHPUnit\Framework\TestCase;

/**
 * Class RequestTest.
 *
 * @package BitAndBlack\RequestCache\Tests
 */
class RequestTest extends TestCase
{
    public function testCanHandleTtl(): void
    {
        $request1 = new Request(
            'https://www.bitandblack.com',
            new DateTime('now +48 hours')
        );

        $request2 = new Request(
            'https://www.bitandblack.com',
            172800
        );

        self::assertSame(
            $request1->getTtl(),
            $request2->getTtl()
        );
    }
    
    public function testCanConvertRequest(): void
    {
        $guzzleRequest = new GuzzleRequest('GET', 'https://www.bitandblack.com');
        
        self::assertFalse(
            method_exists($guzzleRequest, 'setTtl')
        );
        
        $request = Request::createFromHttpRequest($guzzleRequest);

        self::assertTrue(
            method_exists($request, 'setTtl')
        );
    }

    /**
     * @param Request $requestA
     * @param Request $requestB
     * @dataProvider getTestCanDifferentiateKeys
     */
    public function testCanDifferentiateKeys(Request $requestA, Request $requestB): void
    {
        self::assertNotSame(
            $requestA->getKey(),
            $requestB->getKey()
        );
    }

    public function getTestCanDifferentiateKeys(): Generator
    {
        yield [
            new Request(
                'https://www.bitandblack.com',
                60,
                'POST',
                [
                    'headerKey' => 'headerValue',
                ],
                'bodyParameter'
            ),
            new Request(
                'https://www.bitandblack.com',
                60,
                'POST',
                [
                    'headerKey' => 'headerValue',
                ],
                'bodyParameter&anotherBodyParameter'
            ),
        ];

        yield [
            new Request(
                'https://www.bitandblack.com',
                60,
                'GET',
                [
                    'headerKey' => 'headerValue',
                ],
                'bodyParameter'
            ),
            new Request(
                'https://www.bitandblack.com',
                60,
                'POST',
                [
                    'headerKey' => 'headerValue',
                ],
                'bodyParameter'
            ),
        ];
    }
}
