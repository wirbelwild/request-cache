<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache\CacheType;

use BitAndBlack\RequestCache\Request;
use Psr\Log\LoggerAwareInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Interface CacheTypeInterface.
 *
 * @package BitAndBlack\RequestCache
 */
interface CacheTypeInterface extends CacheInterface, LoggerAwareInterface
{
    /**
     * Returns the command for fetching new data in the background.
     *
     * @param Request $request
     * @param CacheTypeInterface $cache
     * @return string
     */
    public function getProcessCommand(Request $request, CacheTypeInterface $cache): string;

    /**
     * Returns if the background command is currently running.
     * This is used for preventing fetching the same data multiple times.
     *
     * @param Request $request
     * @return bool
     */
    public function isProcessRunning(Request $request): bool;

    /**
     * Returns if the cached request has been expired.
     *
     * @param Request $request
     * @return bool
     */
    public function isExpired(Request $request): bool;
}
