<?php

/**
 * Bit&Black Request Cache. Smooth caching of HTTP requested data.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\RequestCache;

use BitAndBlack\RequestCache\CacheType\CacheTypeInterface;

/**
 * The Process class runs PHP processes in the background.
 *
 * @package BitAndBlack\RequestCache
 */
class Process
{
    private string $phpBinaryPath;

    /**
     * Process constructor.
     *
     * @param string|null $phpBinaryPath Path to the PHP binary. If not set, `php` will be used.
     */
    public function __construct(?string $phpBinaryPath = null)
    {
        $this->phpBinaryPath = $phpBinaryPath ?? 'php';
    }

    /**
     * Runs a command.
     *
     * @param string $command
     * @param bool $blocking
     */
    private function runCommand(string $command, bool $blocking = false): void
    {
        if (true === $blocking) {
            exec($this->phpBinaryPath . ' -r "' . $command . '"');
            return;
        }
        
        if (strpos(php_uname(), 'Windows') === 0) {
            $handle = popen('start /B ' . $this->phpBinaryPath . ' -r "' . $command . '"', 'r');
            
            if (is_resource($handle)) {
                pclose($handle);
            }

            return;
        }

        exec($this->phpBinaryPath . ' -r "' . $command . '" > /dev/null &');
    }

    /**
     * Proofs if a process is running and executes a command if not.
     *
     * @param Request $request
     * @param CacheTypeInterface $cache
     * @param bool $waitForResponse
     */
    public function requestNew(Request $request, CacheTypeInterface $cache, bool $waitForResponse = false): void
    {
        $command = $cache->getProcessCommand($request, $cache);
        $isProcessRunning = $cache->isProcessRunning($request);

        if (!$isProcessRunning) {
            $this->runCommand($command, $waitForResponse);
        }
    }
}
